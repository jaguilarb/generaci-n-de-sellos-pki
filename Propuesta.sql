/**
 *	Valida si el régimen asociado al RFC le permite generar sellos digitales
 *
 *	@param cveRegimen	La clave del régimen a validar
 *	@param strObligs	La cadena que contiene las claves de las obligaciones del RFC devueltas por el WS
 *	@param strRoles		La cadena que contiene las claves de los roles del RFC devueltos por el WS
 *	@param strActivs	La cadena que contiene las claves de las actividades del RFC devueltas por el WS
 *
 *	@return true/false
 */

STORE_PROCEDURE (cveRegimen, strObligs, strRoles, strActivs)
BEGIN

	/**
	 *	Valida que el régimen a validar exista
	 */
	regimen = "select count(regimen_cve) from cat_regimen where regimen_cve = " + cveRegimen

	/**
	 *	Si el régimen a validar no existe retorna false
	 */
	IF regimen == 0
		RETURN false

	/**
     *	Si el régimen existe realiza validaciones adicionales
	 */
	ELSE IF regimen > 0

		/**
		 *	Obtiene el numero de roles, obligaciones y actividades asociadas al regimen
		 */
		roles = "select count(rol_cve) from cat_rol where regimen_cve = " + cveRegimen
		obligs = "select count(oblig_cve) from cat_oblig where regimen_cve = " + cveRegimen
		activs = "select count(activ_cve) from cat_activ where regimen_cve = " + cveRegimen

		/**
		 *	El regimen no tiene asociado roles/obligaciones/actividades
		 *
		 *		- No requiere validaciones adicionales
		 */
		IF roles == 0 AND obligs == 0 AND activs == 0
			RETURN true

		/**
		 *	El regimen tiene asociado roles
		 *
		 *		- Valida que algún elemento de la lista strRoles esté en la lista de roles asociados 
		 */
		ELSE IF roles > 0 AND obligs == 0 AND activs == 0
			resultado = "select count(rol_cve) from cat_rol where regimen_cve = " + cveRegimen + " and rol_cve in (" + strRoles + ")"
			RETURN resultado > 0

		/**
		 *	El regimen tiene asociado obligaciones
		 *
		 *		- Valida que algún elemento de la lista strObligs esté en la lista de oligaciones asociadas
		 */
		ELSE IF roles == 0 AND obligs > 0 AND activs == 0
			resultado = "select count(oblig_cve) from cat_oblig where regimen_cve = " + cveRegimen + " and oblig_cve in (" + strObligs + ")"
			RETURN resultado > 0

		/**
		 *	El regimen tiene asociado actividades
		 *
		 *		- Valida que algún elemento de la lista strActivs esté en la lista de actividades asociadas
		 */
		ELSE IF roles == 0 AND obligs == 0 AND activs > 0
			resultado = "select count(activ_cve) from cat_activ where regimen_cve = " + cveRegimen + " and activ_cve in (" + strActivs + ")"
			RETURN resultado > 0

		/**
		 *	El regimen tiene asociado roles/obligaciones
		 *
		 *		- Valida que algún elemento de la lista strRoles esté en la lista de roles asociados
		 *		- Valida que algún elemento de la lista strObligs esté en la lista de obligaciones asociadas
		 */
		ELSE IF roles > 0 AND obligs > 0 AND activs == 0
			resultado1 = "select count(rol_cve) from cat_rol where regimen_cve = " + cveRegimen + " and rol_cve in (" + strRoles + ")"
			resultado2 = "select count(oblig_cve) from cat_oblig where regimen_cve = " + cveRegimen + " and oblig_cve in (" + strObligs + ")"

			RETURN resultado1 > 0 OR resultado2 > 0

		/**
		 *	El regimen tiene asociado roles/actividades
		 *
		 *		- Valida que algún elemento de la lista strRoles esté en la lista de roles asociados
		 *		- Valida que algún elemento de la lista strActivs esté en la lista de actividades asociadas
		 */
		ELSE IF roles > 0 AND obligs == 0 AND activs > 0
			resultado1 = "select count(rol_cve) from cat_rol where regimen_cve = " + cveRegimen + " and rol_cve in (" + strRoles + ")"
			resultado2 = "select count(activ_cve) from cat_activ where regimen_cve = " + cveRegimen + " and activ_cve in (" + strActivs + ")"

			RETURN resultado1 > 0 OR resultado2 > 0

		/**
		 *	El regimen tiene asociado obligaciones/actividades
		 *
		 *		- Valida que algún elemento de la lista strObligs esté en la lista de obligaciones asociadas
		 *		- Valida que algún elemento de la lista strActivs esté en la lista de actividades asociadas
		 */
		ELSE IF roles == 0 AND obligs > 0 AND activs > 0
			resultado1 = "select count(oblig_cve) from cat_oblig where regimen_cve = " + cveRegimen + " and oblig_cve in (" + strObligs + ")"
			resultado2 = "select count(activ_cve) from cat_activ where regimen_cve = " + cveRegimen + " and activ_cve in (" + strActivs + ")"

			RETURN resultado1 > 0 OR resultado2 > 0

		/**
		 *	El regimen tiene asociado roles/obligaciones/actividades
		 *
		 *		- Valida que algún elemento de la lista strRoles esté en la lista de roles asociados
		 *		- Valida que algún elemento de la lista strObligs esté en la lista de obligaciones asociadas
		 *		- Valida que algún elemento de la lista strActivs esté en la lista de actividades asociadas
		 */
		ELSE IF roles > 0 AND obligs > 0 AND activs > 0
			resultado1 = "select count(rol_cve) from cat_rol where regimen_cve = " + cveRegimen + " and rol_cve in (" + strRoles + ")"
			resultado2 = "select count(oblig_cve) from cat_oblig where regimen_cve = " + cveRegimen + " and oblig_cve in (" + strObligs + ")"
			resultado3 = "select count(activ_cve) from cat_activ where regimen_cve = " + cveRegimen + " and activ_cve in (" + strActivs + ")"

			RETURN resultado1 > 0 OR resultado2 > 0 OR resultado3 > 0

END